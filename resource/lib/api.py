import json
from datetime import date
from urllib import quote
from urllib2 import urlopen, Request, HTTPError, URLError

USER_AGENT = 'MypicsApi'

#api url eg   http://107.170.123.126/api/v1/images/
API_URL = 'http://localhost:8000/api/v1'

SYSTEMS = (
    '360', 'PC-CDROM', 'iPhone', 'iPad', 'Android', '3DS', 'NDS', 'Wii_U',
    'PlayStation3', 'PlayStation4', 'PSP', 'PS_Vita', 'Spielkultur',
    'WindowsPhone7', 'XBox', 'Wii', 'PlayStation2',
)

class NetworkError(Exception):
    pass


class MypicsApi(object):
    """
     api class we use to make api calls to django api
    """

    def __init__(self):
        self._system = []

    def set_systems(self, system_list):
        #set up the systems
        self._systems = system_list

    
    def get_images(self, *params):
        "fetch images based on params eg, return_amount=1 , tag=cats,test "

        if not params:
            # params = (
            #     return_amount=25,
            #     tag = 'cats,test'
            #     )
            params = (
                0,  # search_string
                0  # limit
            )

        
        images = self.__api_call('images',params )
        print images
        return images

    @staticmethod
    def __api_call(method, *params):
        parts = [API_URL, method] #+ [quote(str(p)) for p in params]

        url = '/'.join(parts)
        req = Request(url)
        req.add_header('User Agent', USER_AGENT)
        log('Opening URL: %s' % url)
        try:
            response = urlopen(req).read()
        except HTTPError, error:
            raise NetworkError('HTTPError: %s' % error)
        except URLError, error:
            raise NetworkError('URLError: %s' % error)
        log('got %d bytes' % len(response))
        json_data = json.loads(response)
        return json_data    

def log(msg):
    print '[MypicsApi]: %s' % msg



        


