import xbmcaddon
import xbmcgui
from resources.lib.api import MypicsApi, SYSTEMS, NetworkError
import rpdb2 
from xbmcswift2 import Plugin




plugin = Plugin()
api = MypicsApi(plugin)




@plugin.route('/')
def show_root_menu():
    items = [
        {'label': 'Latest Images','path': plugin.url_for('latest_images')},
        {'label': 'Popular Images','path': plugin.url_for('popular_images')},
        {'label': 'Search Images By Tags','path': plugin.url_for('images_by_tags')},

        
    ]
    return plugin.finish(items)

@plugin.route('/latest_images/')
def latest_images():
    page = int(plugin.request.args.get('page', ['1'])[0])
    print "page",page
    images = api.get_images()
    items = __format_images(images)
    print "items", items
    items.append({
        'label': '>> %s >>' % ('next'),
        'path': plugin.url_for(
            endpoint='latest_images',
            page=(page + 1)
        )
    })

    finish_kwargs = {
        'update_listing': 'page' in plugin.request.args
    }
    if plugin.get_setting('force_viewmode', bool):
        finish_kwargs['view_mode'] = 'thumbnail'
    return plugin.finish(items, **finish_kwargs)    

@plugin.route('/popular_images/')
def popular_images():
    page = int(plugin.request.args.get('page', ['1'])[0])
    print "page",page
    images = api.get_images()
    items = __format_images(images)
    print "items", items
    items.append({
        'label': '>> %s >>' % ('next'),
        'path': plugin.url_for(
            endpoint='latest_images',
            page=(page + 1)
        )
    })

    finish_kwargs = {
        'update_listing': 'page' in plugin.request.args
    }
    if plugin.get_setting('force_viewmode', bool):
        finish_kwargs['view_mode'] = 'thumbnail'
    return plugin.finish(items, **finish_kwargs)   

@plugin.route('/images_by_tags/')
def images_by_tags():
 

    tags = str(plugin.keyboard(heading='Search By Tags (separate by comma)'))
    
    params = []
    params.append(('tag',tags))
    
    page = int(plugin.request.args.get('page', ['1'])[0])
    print "page",page
    images = api.get_images(params)
    items = __format_images(images)
    print "items", items
    items.append({
        'label': '>> %s >>' % ('next'),
        'path': plugin.url_for(
            endpoint='latest_images',
            page=(page + 1)
        )
    })

    finish_kwargs = {
        'update_listing': 'page' in plugin.request.args
    }
    if plugin.get_setting('force_viewmode', bool):
        finish_kwargs['view_mode'] = 'thumbnail'
    return plugin.finish(items, **finish_kwargs)  


def __format_images(images):
    
    image_objects = images['objects']

    images = [{
        'label': image['title'],
        'thumbnail': image['image'],
        'info':{'tags':image['tags']},
         'is_playable': True,
         'path':image['image']
    } for i, image in enumerate(image_objects)]
    return images




def __get_enabled_systems():
    
    return SYSTEMS

if __name__ == '__main__':
    try:
        api.set_systems(__get_enabled_systems())
        plugin.run()
    except NetworkError:
        plugin.notify(msg=_('network_error'))